var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/test';

MongoClient.connect(url, function (err, db) {
    if (err) {
        console.log('Невозможно подключиться к серверу MongoDB. Ошибка:', err);
    } else {
        console.log('Соединение установлено для', url);

        var collection = db.collection('user');

        var person = ['Smith', 'Johnson', 'Williams', 'Jones', 'Brown',
            'Davis', 'Miller', 'Wilson', 'Moore', 'Anderson']
            .map(function (item) {
                return {name: item};
            });

        // 1. Добавим список имён в коллекцию;
        collection.insert(person, function (err, result) {
            if (err) {
                console.log(err);
                return;
            }

            // 2. Выведем этот список
            console.log(result.ops);

            // 3. Изменим несколько имён на другие
            newName = 'Doe';
            //collection.update({'name': 'Anderson'}, {'$set': {'name': 'Taylor'}}, {'multi': true}, function (err, result) {
            collection.update({name: /.+ill.+/i}, {$set: {name: newName}}, {multi: true}, function (err, result) {
                if (err) {
                    console.log(err);
                    return;
                }

                // 4. Отобразим изменённый список
                collection.find().toArray(function (err, result) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    console.log(result);

                    // 5. Удалим новые имена из п.3
                    collection.remove({name: newName}, function (err, result) {
                        if (err) {
                            console.log(err);
                            return;
                        }
                        console.log('Удалено документов:', result.result.n, ', содержащих:', newName);

                        // Очистим коллекцию и закроем базу
                        collection.remove();
                        db.close();
                    })
                });
            });
        });
    }
});